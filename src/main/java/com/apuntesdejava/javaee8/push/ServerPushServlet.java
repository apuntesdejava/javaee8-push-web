package com.apuntesdejava.javaee8.push;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.PushBuilder;

/**
 *
 * @author diego.silva (diego.silva@apuntesdejava.com)
 */
@WebServlet(name = "ServerPushServlet", urlPatterns = {"/ServerPushServlet"})
public class ServerPushServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PushBuilder pb = request.newPushBuilder();
        if (pb != null) {
            //redirecciono las peticiones como Push
            pb.path("images/javaee-logo.png")
                    .addHeader("Content-type", "image/png")
                    .push();
            pb.path("js/jquery.min.js")
                    .addHeader("Content-type", "text/javascript")
                    .push();
        }
        response.setContentType("text/html");
        //cargo el html para mostrarlo en el cliente
        RequestDispatcher rd = this.getServletContext().getRequestDispatcher("/html/view.html");
        rd.include(request, response);

    }

}
